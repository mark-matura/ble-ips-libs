#include "displayHelper.h"
#include "pinDefinitions.h"

#include <TFT_eSPI.h>

/*
    This project uses the tft_espi display by Bodmer to log directly to tft a output screen.
    https://github.com/Bodmer/TFT_eSPI
*/

TFT_eSPI display = TFT_eSPI(135, 240); // Invoke custom library and set resolution

void resetScreen() {
    // cursor at beginning and select font 1
    display.setCursor(0, 0, 1);

    // make text color white on black background
    display.fillScreen(TFT_BLACK);
    display.setTextColor(TFT_WHITE,TFT_BLACK);
}

void initializeDisplay() {
    display.init(); // begin session with Bodmer's display_espi library
	display.setTextWrap(true); // text should wrap at end of line

    // set flipped landscape mode
    display.setRotation(3);

    display.setTextSize(1);


    resetScreen();

    //strings are printed relative to the bottom left of screen
    display.setTextDatum(TL_DATUM);

    if (TFT_BL > 0) { // TFT_BL has been set in the TFT_eSPI library in the User Setup file TTGO_T_Display.h
         pinMode(TFT_BL, OUTPUT); // Set backlight pin to output mode
         digitalWrite(TFT_BL, TFT_BACKLIGHT_ON); // Turn backlight on
    }
}

void printMsg(std::string message) {
    display.println(message.c_str());
    display.setTextColor(TFT_WHITE,TFT_BLACK);

}

void printError(std::string errorMessage) {
    display.setTextColor(TFT_RED,TFT_BLACK);
    printMsg(errorMessage);

}

void printGreen(std::string message) {
    display.setTextColor(TFT_GREEN,TFT_BLACK);
    printMsg(message);
}

void printCyan(std::string message) {
    display.setTextColor(TFT_CYAN,TFT_BLACK);
    printMsg(message);
}

void printRSSI(int rssi) {
    display.println(rssi);
}