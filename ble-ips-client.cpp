#include "ble-ips-client.h"
#include ".env.h"
#include "displayHelper.h"

#include <WiFiMulti.h>
#include <string>
#include <HTTPClient.h>
#include <ArduinoJson.h>

using namespace std;

// get the service UUID that is propagated across Server and Clients via the library
BLEUUID serviceUUID(SERVICE_UUID);

WiFiMulti wifiMulti;

// const ref for read only access
int postDataToServer(const std::string &deviceAddress) {
    HTTPClient http;

    http.begin("ble-ips-api.tenderribs.cc/api/v1/server/store");
    http.addHeader("Content-Type", "application/json");

    StaticJsonDocument<200> doc;
    doc["mac_addr"] = deviceAddress;

    String requestBody;
    serializeJson(doc, requestBody);

    int httpResponseCode = http.POST(requestBody);

    // if (httpResponseCode > 0) {
    //     std::string response = http.getString().toString();
    //     printMsg(response);
    // }

    return httpResponseCode;
}

// callback for discovery of a BLE Device
class onDeviceDiscovery: public BLEAdvertisedDeviceCallbacks {
    void onResult(const BLEAdvertisedDevice &advertisedDevice) {
     // check if service UUID match our beacons
        if (advertisedDevice.haveServiceUUID() && advertisedDevice.isAdvertisingService(serviceUUID)) {
            resetScreen();

            printMsg("Device Address:");
            printMsg(advertisedDevice.getAddress().toString());

            printMsg("RSSI:");
            printRSSI(advertisedDevice.getRSSI());

            int response =  postDataToServer(advertisedDevice.getAddress().toString());
            printRSSI(response);
        }
    }
};

BLEScan* getScanner() {
    printGreen("Scanner Started");
    BLEDevice::init("BLE IPS CLIENT");

    BLEScan* scanner = BLEDevice::getScan();

    scanner->setAdvertisedDeviceCallbacks(new onDeviceDiscovery());
    scanner->setInterval(1349);
    scanner->setWindow(449);
    return scanner;
}

void connectToWifi() {
    wifiMulti.addAP(WIFI_SSID, WIFI_PASSWORD);
    printMsg("Connecting to Wifi");

    while(wifiMulti.run() != WL_CONNECTED) {
        printMsg("Connecting ...");
        delay(500);
    }

    printGreen("Connected to Wifi");
}

