#ifndef DISPLAY_HELPER_H
#define DISPLAY_HELPER_H
#include <TFT_eSPI.h>
#include <string>

/**
 * reset screen
 *
 * fill screen with black color and clear all lines
 * then set text color to white font on black background
 *
 * @return void
 */
void resetScreen();

/**
 * Initialize TFT_eSPI
 *
 * initialize a display instance member in displayHelper
 * and configure colors and font
 *
 * @return void
 */
void initializeDisplay();

/**
 * print a message to display
 *
 * print a simple message with regular font and text color
 *
 * @param message std::string to be printed
 * @return void
 */
void printMsg(std::string message);

/**
 * print an error message to display
 *
 * print message with red font
 *
 * @param message std::string to be printed
 * @return void
 */
void printError(std::string errorMessage);

/**
 * print a green message to display
 *
 * print message with green font
 *
 * @param message std::string to be printed
 * @return void
 */
void printGreen(std::string message);

/**
 * print a cyan message to display
 *
 * print message with cyan font
 *
 * @param message std::string to be printed
 * @return void
 */
void printCyan(std::string message);

void printRSSI(int rssi);
#endif